---
title: "Chocolat Apples and Bananas: a fire camp dessert"
date: 2020-09-13T00:00:00+01:00
author: "Constance"
draft: false
---

![](/tutorials/chocolat-apples-and-bananas/image001.jpg)

## What you need:
- One banana or apple per person
- One bar of dark cooking chocolate (even if you usually prefer milk chocolate, the sugar of the fruits complements the slight bitterness of the chocolate)
- A knife and cutting board to cut the fruits
- Aluminium foil to wrap the fruits
- A camp fire (a barbecue or an oven can also do the trick but without the camp vibe)

## If you choose to use a banana :

![](/tutorials/chocolat-apples-and-bananas/image002.jpg)

- Slit the banana in the length direction, about the quarters through

![](/tutorials/chocolat-apples-and-bananas/image003.jpg)
- Add the chocolate bar in the slit

## If you choose to use an apple:

![](/tutorials/chocolat-apples-and-bananas/image004.jpg)
- Wash the apple. You can peel it, but it's not mandatory 

![](/tutorials/chocolat-apples-and-bananas/image005.jpg)
- Remove the core of the apple

![](/tutorials/chocolat-apples-and-bananas/image006.jpg)
- Add the chocolate cut into square in the middle of the apple

![](/tutorials/chocolat-apples-and-bananas/image008.jpg)
- Wrap the banana and/or the apple in the aluminium foil (careful not to leave holes)

![](/tutorials/chocolat-apples-and-bananas/image009.jpg)
- Bake into the oven at 180 degrees for an half hour

![](/tutorials/chocolat-apples-and-bananas/image010.jpg)
![](/tutorials/chocolat-apples-and-bananas/image011.jpg)
- Open the aluminium foil and transfer the fruits on a plate

![](/tutorials/chocolat-apples-and-bananas/image012.jpg)
- Eat the inside of the banana, leaving the peel or eat the whole apple