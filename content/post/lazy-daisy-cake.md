---
title: "Lazy Daisy Cake/Cupcakes"
date: 2020-09-13T00:00:00+01:00
author: "Tamsyn"
draft: false
---

**Cake Ingredients:**

- 3 Eggs
- 190g Margarine/Butter
- 550ml Self-raising Flour
- 290ml Sugar
- ½ tsp Salt
- 1 tsp Vanilla Essence
- 2 tbsp Cocoa (Optional)
- 200ml Milk
- Caramel (Optional)

**Icing Ingredients:**

- 500g Icing Sugar
- 3 tbsp Margarine/Butter
- 1 tsp Vanilla Essence
- 1 tbsp Cocoa (Optional)
- Milk

![](/tutorials/lazy-daisy-cake/image001.jpg)

**Cake Method:**

- Crack eggs into a mixing bowl

![](/tutorials/lazy-daisy-cake/image002.jpg)

- Throw all other ingredients in the mixing bowl leaving the milk until last
- Beat/Mix all ingredients together

![](/tutorials/lazy-daisy-cake/image003.jpg)

![](/tutorials/lazy-daisy-cake/image004.jpg)

![](/tutorials/lazy-daisy-cake/image005.png) ![](/tutorials/lazy-daisy-cake/image007.png)

![](/tutorials/lazy-daisy-cake/image009.jpg)

- If making cake, divide the mixture into 2 well-greased baking tins
- If making cupcakes, spoon mixture into well-greased cupcake/muffin tins
- If making cake, bake at 180⁰C for 30 minutes
- If making cupcakes, bake at 180⁰C for 20-25 minutes
- To check that the cake/cupcakes are done stick a knife in the middle, if it comes out clean the cake/cupcakes are fully cooked
- Allow to cool
- If making a cake you can put caramel in-between the two cake halves, alternatively you can put a layer of icing in-between

**Icing Method:**

- Add the icing sugar into a mixing bowl
- Add the butter/margarine and vanilla essence

![](/tutorials/lazy-daisy-cake/image010.jpg)

![](/tutorials/lazy-daisy-cake/image011.jpg)

- Add cocoa if making chocolate icing

![](/tutorials/lazy-daisy-cake/image012.jpg)

- Add some milk a little at a time until you reach the required consistency
- If you add too much milk, just add more icing sugar to balance it out

![](/tutorials/lazy-daisy-cake/image013.jpg)

![](/tutorials/lazy-daisy-cake/image014.jpg)