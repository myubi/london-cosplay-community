---
title: "Crepes the French Way"
date: 2020-09-12T00:00:00+01:00
author: "Constance"
draft: false
---

## You will need:

- 250g of flour 
- 3 eggs
- 50g of salted butter
- 0.5l of milk + half a glass of water

## How to make the crepes:

Pour the flour in a large bowl. Make a well in the flour and add the eggs, add the melted butter and finally the milk and the water (either mix it by adding small quantities at a time to avoid lumps or just use a blender). 

<div style="display: flex; justify-content: center;">
<iframe width="560" height="315"
src="https://www.youtube.com/embed/xMO-X-5RYz4" 
frameborder="0" 
allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" 
allowfullscreen></iframe>
</div>

Make your crepes by putting a small quantity of the mix in a hot non-stick pan: your crepe should be quite thin. You need to turn your crepe when there is no liquid left and it detaches itself easily from the pan.
Add the fillings as soon as you turn your crepe so that they can melt and/or cook slightly. Fold in the pan and cook until your crepe is nice and gold.
Eat immediately while the crepe is still hot.

A few filling ideas:

- tomato, goat cheese, honey and basil (or thyme) 
- grated cheese, ham and egg
- jam
- chocolate 
- sugar and salted butter
- salted caramel sauce
- apple and almonds
