---
title: "How to Sew Buttons"
date: 2020-09-12T00:00:00+01:00
author: "Constance"
draft: false
---

## You will need:

- Thread
- Buttons
- Fabric

## See the step-by-step on the video below!

<div style="display: flex; justify-content: center;">
<iframe width="560" height="315"
src="https://www.youtube.com/embed/L7lk2fvMkRY" 
frameborder="0" 
allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" 
allowfullscreen></iframe>
</div>