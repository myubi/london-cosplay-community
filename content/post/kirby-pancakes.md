---
title: "Kirby Pancakes"
date: 2020-09-12T00:00:00+01:00
author: "Chalisa"
draft: false
---

## You will need:

- 2 large eggs (50 g each w/o shell)
- 1 ½ Tbsp whole milk (22 g)
- ¼ tsp pure vanilla extract
- ¼ cup cake flour (30 g)
- ½ tsp baking powder (2 g)
- 2 Tbsp sugar (25 g)
- 1 Tbsp neutral-flavored oil (vegetable, canola, etc) (for greasing the pan)
- 2 Tbsp water (for steaming)
- Fresh Whipped Cream (optional)
- ½ cup heavy (whipping) cream (120 ml)
- 1 ½ Tbsp sugar (20 g)
- 1 Tbsp confectioners’ sugar/powder sugar
- 10g of white and milk chocolate
- Red food colouring
- A handful of strawberries

## See the step-by-step on the video below!

<div style="display: flex; justify-content: center;">
<iframe width="560" height="315"
src="https://www.youtube.com/embed/vREukMn43Js" 
frameborder="0" 
allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" 
allowfullscreen></iframe>
</div>