---
title: "Bag of Holding"
date: 2020-09-13T00:00:00+01:00
author: "Taty"
draft: false
---

## For this craft, you will need:

- 2 Cotton Fabrics (ideally you want one patterned and one simple, but that is totally up to you! one for the inside and one for the outside)

Of each fabric color you want:

- 1x 16.5cm Radius Circle
- 1x 12cm Radius Circle

- Dacron Padding, on a circular shape of 4cm Radius

- 1m Thin Satin Rope

## See the step-by-step on the video below!

<div style="display: flex; justify-content: center;">
<iframe width="560" height="315"
src="https://www.youtube.com/embed/P2eB7yneEAU" 
frameborder="0" 
allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" 
allowfullscreen></iframe>
</div>