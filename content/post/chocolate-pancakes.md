---
title: "Chocolate Pancakes"
date: 2020-09-13T00:00:00+01:00
author: "Marion"
draft: false
---

## Ingredients:

- 200g of flour 
- 250g of Milk (I buy small bottle at Sainsbury and use 2/3. But it is more a way to control how liquid the pastry/dough will be)
- 20g of cocoa powder
- 1 tea spoon of baking soda powder 
- 10g of sugar
- 1 tea spoon of vanilla extract (optional)
- 100g of chocolate chips (Milk or dark, whatever you want !! And you can put less or more than 100g of course!)

## Recipe:

1: Once you have all your ingredient, the first step is to put together in a big bowl, all the dry one: flour, baking powder, sugar, chocolate chips (100g or more, or less, as you wish!), cocoa powder.

![](/tutorials/chocolate-pancakes/image001.png)

2: Then you just have to mix them, easy one!

![](/tutorials/chocolate-pancakes/image002.png)

3: Next, you just have to add the vanilla extract (optional) and the milk. The recipe says 250g (g??? Hello, where are the ml?) but I found it too thick, so as a reference I put 2/3 of the small milk bottle you can find at Sainsbury&#39;s. If you have a measuring bowl, I will take the part were you measure &#39;sugar&#39; and put between 300g or 350g, depends on you, on how thick you want your dough.

![](/tutorials/chocolate-pancakes/image003.png)

3: Once this is ready, heat a frying pan with a bit of oil in it (olive or coconut, as you wish) and wait until it is warm. Then put some dough in it with a spoon and you can use it to give to your pancake the shape you want:

![](/tutorials/chocolate-pancakes/image004.png)

4: Wait for your pancake to cook and when you can see small bubbles, you can tun it over with a spatula: at this point I will keep everything at medium heat in order to have more control on the cooking process

![](/tutorials/chocolate-pancakes/image005.png) ![](/tutorials/chocolate-pancakes/image006.png) 

5: Then the rest is pretty much your choice! The number of pancakes will depend on how big you will make them and how much of dough you will put in the frying pan for one! In general, I can have 12 of them.

![](/tutorials/chocolate-pancakes/image007.png) 6: When you have no more dough, it means that everything is ready and you can EAT THEEEEEM! Yum yum! You can choose to put berries with (raspberries, blackberries, banana as well, whatever you want, you are the master here ah ah) and maybe a sauce? Maple sirup, caramel or chocolate sauce, just listen to your imagination and ENJOY =)