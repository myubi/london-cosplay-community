---
title: "Cactus Pincushion"
date: 2020-09-12T00:00:00+01:00
author: "Taty"
draft: false
---

## For this craft, you will need:

- Felt sheets (preferably green and brown but any color you want! You can also use accent colors for flowers!)
- Clay/Terra Cotta/Wood/Plastic little pots. You can also make those out of felt!
- Needle
- Embroidery Thread
- Soft Stuffing (usually Toys/Plush are the best)
- Hot Glue Gun
- Fabric Scissors
- Doll Eyes, Beads or Buttons (optional, only if you want to make cute faces!)

## See the step-by-step on the video below!

This tutorial is inspired by A Bautiful Mess and Delilah Iris tutorials. 

Don't forget to download the [Cacti Pattern](/files/Cacti.pdf) and the [Flower Pot Pattern](/files/Base.pdf)

<div style="display: flex; justify-content: center;">
<iframe width="560" height="315"
src="https://www.youtube.com/embed/v0YpM_y6Esk" 
frameborder="0" 
allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" 
allowfullscreen></iframe>
</div>