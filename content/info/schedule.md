---
title: Schedule
subtitle: ""
date: 2020-08-29
tags: []
---

![Camp Schedule](/img/camp-schedule.jpg)

Activities without a time mean they will be published here on our site at 9am on the day specified on the schedule and can be done at any time!

Closet Cosplay is a themed cosplay challenge where you dress up based on a theme with clothes you already own, including previous cosplays!

Dungeons and Dragons will required pre-registration, which will open on the 5th of September