---
title: "Supplies List"
subtitle: ""
date: 2020-08-30
tags: []
---

## Crepes: The French Way

For the base:

- 250g of flour 
- 3 eggs
- 50g of salted butter
- 0.5l of milk + half a glass of water

For the fillings, it is up to you!  
A few ideas:  

- tomato, goat cheese, honey and basil (or thyme) 
- grated cheese, ham and egg
- jam
- chocolate 
- sugar and salted butter
- salted caramel sauce
- apple and almonds

------------

## Chocolat apples and bananas: a fire camp dessert

Per person:

- One apple or banana 
- One bar of dark cooking chocolate
- Kitchen foil
- A camp fire (a barbecue or an oven can also do the trick but without the camp vibe)

------------

## Lazy Daisy Cake/Cupcakes

Cake Ingredients:

- 3 Eggs
- 190g Margarine/Butter
- 550ml Self-raising Flour
- 290ml Sugar
- ½ tsp Salt
- 1 tsp Vanilla Essence
- 2 tbsp Cocoa (Optional)
- 200ml Milk
- Caramel (Optional)

Icing Ingredients:

- 500g Icing Sugar
- 3 tbsp Margarine/Butter
- 1 tsp Vanilla Essence
- 1 tbsp Cocoa (Optional)
- Milk

------------

## How to sew buttons

- Thread
- Buttons
- Fabric

------------

## Bag of Holding tutorial 

- 2 Cotton Fabrics (ideally you want one patterned and one simple, but that is totally up to you! one for the inside and one for the outside)

Of each fabric color you want:

- 1x 16.5cm Radius Circle
- 1x 12cm Radius Circle

- Dacron Padding, on a circular shape of 4cm Radius

- 1m Thin Satin Rope

------------

##  Make your own Cactus Pincushion

- Felt sheets (preferably green and brown but any color you want! You can also use accent colors for flowers!)
- Clay/Terra Cotta/Wood/Plastic little pots. You can also make those out of felt!
- Needle
- Embroidery Thread
- Soft Stuffing (usually Toys/Plush are the best)
- Hot Glue Gun
- Fabric Scissors
- Doll Eyes, Beads or Buttons (optional, only if you want to make cute faces!)

------------

## Kirby Sweets

- 2 large eggs (50 g each w/o shell)
- 1 ½ Tbsp whole milk (22 g)
- ¼ tsp pure vanilla extract
- ¼ cup cake flour (30 g)
- ½ tsp baking powder (2 g)
- 2 Tbsp sugar (25 g)
- 1 Tbsp neutral-flavored oil (vegetable, canola, etc) (for greasing the pan)
- 2 Tbsp water (for steaming)
- Fresh Whipped Cream (optional)
- ½ cup heavy (whipping) cream (120 ml)
- 1 ½ Tbsp sugar (20 g)
- 1 Tbsp confectioners’ sugar/powder sugar
- 10g of white and milk chocolate
- Red food colouring
- A handful of strawberries

-----------

# Chocolate Pancakes

- 200g of flour 
- 250g of Milk (I buy small bottle at Sainsbury and use 2/3. But it is more a way to control how liquid the pastry/dough will be)
- 20g of cocoa powder
- 1 tea spoon of baking soda powder 
- 10g of sugar
- 1 tea spoon of vanilla extract (optional)
- 100g of chocolate chips (Milk or dark, whatever you want !! And you can put less or more than 100g of course!)

