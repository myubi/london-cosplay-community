---
title: "Day 2"
date: 2020-09-13T00:00:00+01:00
draft: false
---

Good mooooooooooorning campers! Welcome to the second (yay) and final (boo) day of LCC Camp!

![Alt Text](https://media.giphy.com/media/DBqr5JfVORb56/giphy.gif)

Are you ready for more activities today? 

If you’ve only just joined us, there is still time to get involved! Hop along to our [Discord](https://discord.gg/EDrWTDS)!

## Morning announcement

<audio
		controls
		src="/audio/Announcement_02_final.mp3">
				Your browser does not support the
				<code>audio</code> element.
</audio>

## Activities

We have some more fun activities today! If you missed the first post, you can find them [here](/summary/day-1)

## Ongoing Activities

![Alt Text](https://media.giphy.com/media/eNMtqDiX4CIUwtB0qh/giphy.gif)

## Blanket fort! 

If you haven’t made your blanket fort yet there is still time! Grab those pillows and show us the cosiest place you made! Share it on your social media with the hashtag #LCCCamp or on our discord server! 

![Alt Text](https://media.giphy.com/media/26Ff1AuTbTEm2Kc9O/giphy.gif)

## Badge Collection 

Still have a few more badges to collect? Fret not, you can collect them until midnight today! If you are not sure what I am talking about check back [here](/summary/day-1) on our Day 1 post!

## Non Timed Activities

Closet Cosplay Challenge: MAGIC
Using clothes and/or costumes you already own, dress up as the most magical being you can be! Share it on your social media with the hashtag #LCCCamp or on our discord server! 

The following activities have their own blog posts, click on them to find out more about them!
- [The Lazy Daisy Cake/Cupcake](/post/lazy-daisy-cake) 
- [Chocolat Apples and Banana: A campfire dessert](/post/chocolat-apples-and-bananas) 
- [Chocolate Pancakes](/post/chocolate-pancakes) 
- [Bag of Holding Tutorial](/post/bag-of-holding) 

## Timed Activities

If you have some time today, please join us for the following activities, all happening on our Discord server! 

PLEASE NOTE: the Dungeons and Dragons adventure required sign up beforehand so, if you didn’t manage to get a place as a player, you are welcome to join us as a spectator! 

**15:00** - Jackbox Games <br />
**18:00** - Dungeons and Dragons <br /> 
**21:00** - Movie Night: Lord of the Rings: Spirited Away

That is all for now, folks!

Thank you for joining us for camp again today! We hope you had a great time! 

LCC Camp may be ending at midnight tonight but that doesn’t mean you can’t keep that magical energy glowing from you! Go forth and shine!

Have a good day campers!

Counselor Eevee
