---
title: "Day 1"
date: 2020-09-12T00:00:00+01:00
draft: false
---

Good mooooorning campers, how are you today?

![Alt Text](https://media.giphy.com/media/d0THNFbZHEV4k/giphy.gif)

We know 2020 hasn’t been the best year, or the year we exactly expected; so many plans, ideas and adventures had to go on hold until whenever-o’clock (not cool). But that is why we have to get together, in any way we can!

Cosplay is usually a very sociable activity; you want to be with your friends, out and about in the great wide world, being photographed, dancing the Macarena and generally having a great time IRL. We can’t do a lot of this right now, unfortunately, but that doesn’t mean we can’t have fun!

With that in mind, I am proud to announce that our first ever London Cosplay Community Camp starts TODAY! We wanted to show everyone that, even in times like these, we can still create a fun community together. Our doors will always be open to welcome you!

This is our first time doing an event like this, so bear with us on getting everything up to speed. Our little team did plenty of planning and prepared a lot of things in advance, but that doesn’t mean things will go 100% smoothly. It is a camp, there might be bugs… 

We hope you have fun nonetheless!

Speaking of our team, allow me to introduce you to your Camp Counsellors:

- Counsellor Eevee (Taty)
- Counsellor SugarPopple (Tamsyn)
- Counsellor Chaos (Jack)
- Nurse Sparkles (Ed)

Need some more info? Have a burning question? Just want to get to camp? Then join the LCC Camp chats on our Discord channel [HERE](https://discord.gg/EDrWTDS)

Without further ado, here are our morning announcements!

## Morning announcement

<audio
		controls
		src="/audio/Announcement_01_final.mp3" type="audio/mp3">
</audio>

## Nurse Sparkles

Thank you Nurse Sparkles, your happy voice always makes the sun shine a little brighter. 

Nurse Sparkles is here to make your day more magical! 

If your day is more MEH than YAY and/or you need a reminder of just how awesome you are, head over to the Nurse Sparkles cabin on Discord and have a friendly chat.

He can also provide you with a RLO (real life obligations) “sick note” so that you can enjoy camp and disregard other real life obligations. Just tell him who is the recipient of the sick note (eg. parent, sibling, boss, extra-dimensional bed monster…), what you wish to be excused from and why you will be busy!

## Activities

A virtual camp wouldn’t be a camp if there weren't some activities!

We have broken down our activities into three types: [ongoing], [non-timed activities] and [timed activities]. Ongoing activities will happen from today until the camp ends. Non-timed activities are announced each morning with this blog post and can be done at any time, even after camp ends. Timed activities require you to be present and online at the time the activity is happening.

## Ongoing Activities

![Alt Text](https://media.giphy.com/media/yV3BYKlfL85MY/giphy.gif)

### Blanket fort! 
It is cold and muddy outside, so instead we are camping with style in our bedrooms! Make a blanket fort and take pictures of it! Share it on your social media with the hashtag #LCCCamp or on our discord server! 

![Alt Text](https://media.giphy.com/media/3oEhn6ugvvOuIXex56/giphy.gif)

### Badge Collection 
How about getting a nice memento from your time at camp? If you want to collect some badges, here are the requirements for them!

Once you fill a requirement for a badge, ping Counselor Eevee who will send you your virtual badge! You can then print it and even make stickers out of it! If you are into more fancy ways of collecting badges, you can also download the app Badge Wallet and have them virtually stored there!

**BUDDY BADGE** – Make a new friend/new friends at camp <br />
**CRAFTY BADGE** – Create and post at least one of the crafts taught during camp <br />
**TASTY BADGE** – Create and post at least one of the recipes taught during camp <br />
**ADVENTURE BADGE** – Complete the D&D Adventure <br />
**CHATTER BADG** – Start or engage in conversations (spoken or written) <br />
**APOCALYPSE BADGE** – Take part in the APOCALYPSE Closet Cosplay activity <br />
**MAGICAL BADGE** – Take part in the MAGIC Closet Cosplay activity <br />
**SPARKLE BADGE** – Any camp member can award this to members who added sparkle to their day (just let Counsellor Eevee know who you wish to award this badge to and why) <br />
**NURSE BADGE** – Visit Nurse Sparkles at least once during camp <br />
**DETECTIVE BADGE** – Find out each camp counsellors’ favourite movie or series! <br />
**MONSTER BADGE** – Submit a drawing of what you think the ‘thing in the lake’ looks like.  <br />
**MOVIE BADGE** - Join one of the movie nights session!

If you collect all the badges during camp time, you will also get a super secret extra badge! :D

## Non Timed Activities

Closet Cosplay Challenge: APOCALYPSE 
Using clothes and/or costumes you already own, dress up ready for the apocalypse! Share it on your social media with the hashtag #LCCCamp or on our discord server! 

The following activities have their own blog posts, click on them to find out more!

- [Crepes: The French Way](/post/crepes-the-french-way) 
- [Kirby Pancakes](/post/kirby-pancakes)
- [How to Sew Buttons](/post/how-to-sew-buttons)
- [Make your own Cactus Pincushion](/post/cactus-pincushion)

## Timed Activities

If you have some time today, please join us for the following activities, all happening on our Discord server!

**15:00** - Jackbox Games <br />
**18:00** - Avalon Game <br /> 
**21:00** - Movie Night: Lord of the Rings: The Fellowship of the Ring

That is all for now, folks!

Thank you for joining us today! As a last treat, we have a little camp song that you can sing along through the weekend:

<audio
		controls
		src="/audio/LCCCAMPSONG2020.m4a">
				Your browser does not support the
				<code>audio</code> element.
</audio>

**Lyrics:**

Welcome to camp LCC <br />
Come along and sing with me <br />
We’ll make sweet treats, play fun games <br />
Watch some movies, make new friends. <br />
Come along to camp today <br />
Have some fun and shout HOORAY!

Have a good day campers! 

Counselor Eevee

