---
title: ""
---

## Welcome to the London Cosplay Community Camp!

This is a free event happening on the 12th and 13th of September 2020 on our [Discord](https://discord.gg/EDrWTDS)!

Register here: https://cutt.ly/lcc-camp

Find more info below!
